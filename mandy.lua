#!/usr/bin/env lua

require("iuplua")
require("iupluacontrols")
require('progress')
require('ffmpeg_progress')

--Main url entry box
function url_entry()
  local format = sel_format()
  local bitRate = sel_bitrate()
  local url = ""
  local playNow = sel_play()
  local changeLog = "* Set the default settings in 'mandy.cfg'\n"..
                    "* Can now select audio bitrate\n"..
                    "* Autoremove old video files from /tmp\n"..
                    "* Added option to play completed file\n"..
                    "- Still unknown bug with 'converting' bar\n"
  

  res, url, format, bitRate, playNow, changeLog = iup.GetParam("Mandy 0.6.0 (alpha)", nil,
                          "Enter Youtube URL: %s\n"..
                          "Audio Options %t\n"..
                          "Select an audio format: %l|mp3|ogg|flac|\n"..
                          "Select the bitrate: %l|320 kbps|192 kbps|160 kbps|128 kbps|96 kbps|\n"..
                          "Play file when finished?: %l|no|yes|\n"..
                          "Changelog: %m\n", 
                          url, format, bitRate, playNow, changeLog)

  gPlay = ask_play(playNow)
  gFormat = sel_Codec(format)
  gBitrate = bit_logic(bitRate)
  if res == true then
    if string.match(url, "youtube.com/watch") then
      return url
    else
      iup.Message("Error", "The URL is not a youtube link")
      iup.Flush()
      iup.Close()
    end
  else
    return false
  end
end


-- checks mandy.cfg for format
function sel_format()
  local format = cfg_table["format"]
  if format == "mp3" then return 0 
  elseif format == "ogg" then return 1
  elseif format == "flac" then return 2
  else return 0
 
  end
end

function sel_bitrate()
  local bitrate = cfg_table["bitrate"]
  if bitrate == "320k" then return 0
  elseif bitrate == "192k" then return 1
  elseif bitrate == "160k" then return 2
  elseif bitrate == "128k" then return 3
  elseif bitrate == "96k" then return 4
  elseif bitrate == "32k" then return 5
  else return 0
  end
end

function sel_play()
  local playFile = cfg_table["playFile"]
  if playFile == "no" then return 0
  elseif playFile == "yes" then return 1
  else return 0
  end
end



--make format dropdown menu items work
function sel_Codec(x)
  if x == 0 then
    local mp3 = ".mp3"
    print("Selected "..mp3)
    return mp3
  elseif x == 1 then
    local ogg = ".ogg"
    print("Selected "..ogg)
    return ogg
  elseif x == 2 then
    local flac = ".flac"
    print("Selected "..flac)
    return flac
  end

end

function bit_logic(x)
  if x == 0 then
    local b320k = "320k"
    print ("Selected "..b320k)
    return b320k
  elseif x == 1 then
    local b192k = "192k"
    print ("Selected "..b192k)
    return b192k
  elseif x == 2 then
    local b160k = "160k"
    print ("Selected "..b160k)
    return b160k
  elseif x == 3 then
    local b128k = "128k"
    print ("Selected "..b128k)
    return b128k
  elseif x == 4 then
    local b96k = "96k"
    print ("Selected "..b96k)
    return b96k
  end
end

--Ask user to set directory
function set_dir()

  getPath = iup.filedlg{dialogtype = "SAVE", title="Save file...",
                        filter="*.mp3; *.ogg; *.flac",
                        directory=".",
                        file=""}

  getPath:popup(iup.ANYWHERE, iup.ANYWHERE)

  local status = getPath.status

  -- The "%" is used to escape the "." comming from gFormat
  if status == "1" or status == "0"  then
    if string.match(getPath.value, "%"..gFormat) then
      local savedPath = getPath.value
      return savedPath

    else
      local savedPath = getPath.value..gFormat
      return savedPath

    end
    
  elseif status == "-1" then
    return false
  end
end

function ytDl(x)
  local tmpName = math.random(0,999999999999)
  local tmpNameTwo = math.random(0,999999999999)
  local tmpPath = x.." --output=/tmp/youtube-dl-"..tmpName.."-"..tmpNameTwo..".flv"
  local tmpPath2 = "/tmp/youtube-dl-"..tmpName.."-"..tmpNameTwo..".flv"
  print(x)
  local flag = popin.ytdl(tmpPath, tmpPath2)
  if not flag then return false end
  
  return tmpPath2
end

--optional function to implement autoplay

function ask_play(x)
  if x == 0 then
    return false
  elseif x == 1 then
    return true
  end

end


function ffmpeg(tmpPath, dirD)
  
   if gFormat == ".mp3" then gFormat = "libmp3lame" end
   if gFormat == ".ogg" then gFormat = "libvorbis" end 
   if gFormat == ".flac" then gFormat = "flac" end
  local subSymbols = string.gsub(dirD, "'", "") 
  local subSpaces = string.gsub(subSymbols, " ", "\\ ")
  local codec = "ffmpeg -i "..tmpPath.." -acodec "..gFormat.." -ac 2 -ab "..gBitrate.." -vn -y "..subSpaces

  local flag = convert_mp3.go(codec, tmpPath)
    if not flag then return false end

  io.popen("notify-send 'The file was saved to' "..subSpaces)
  io.close()
  if gPlay == true then
    io.popen("xdg-open "..subSpaces)
    io.close()

  elseif gPlay == false then
    print ("You chose not to open the file")
  end
end

--put everything into a table to use local functions globally
global = {}
global.url = function ()
  local url = url_entry()
  return url
end

global.dir = function()
  local dir = set_dir()
  return dir
end

global.path = function(url)
  local yt = ytDl(url)
  return yt
end

global.ffmpeg = function(tmpPath, saveDir)
  local convert = ffmpeg(tmpPath, saveDir)
  return convert
end


