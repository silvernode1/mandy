require ('iuplua')

local cancelflag		
local downloadProgress		


--create button with cancel flag
local function downloadStart(YtLink)		
  cancelButton = iup.button{
                            title = "Cancel",
                            action = function()	      
                              cancelflag = true         
                          end}
  
  downloadProgress = iup.progressbar{expand="HORIZONTAL"}	
  dlgProgress = iup.dialog{				   
                           title = "Downloading.. ",
                           dialogframe = "YES", border = "YES",
                           iup.vbox {			
                                     downloadProgress,
                                     cancelButton,
                                     pTest,
                                    }
                          }
  dlgProgress.size = "QUARTERxEIGHT" 		
  dlgProgress.menubox = "NO"		-- no windows menus
  dlgProgress.close_cb = cancelButton.action	
  dlgProgress:showxy(iup.CENTER, iup.CENTER)	
  return dlgProgress
end


-- start download, create progress bar, monitor progress
local function DL_progress(YtLink, tmpPath)  
  downloadStart(YtLink) 		  
  downloadProgress.value = 0  
  local pipe = io.popen("youtube-dl " ..YtLink )
  repeat
    local c = pipe:read(20)	  -- read the first 20 characters of each line
    if c then 		
      local   b = string.match(c, '.....%%')	  

      if b then
        local b = string.sub(b,0,-2)
        downloadProgress.value = b/100 -- x out of 100
        iup.Flush()		    
        iup.LoopStep() --check for user input
        iup.Flush() 
        if cancelflag then  
          iup.Message("Terminated", "You have canceled the download") 
          iup.Flush()
          pipe:close()
          os.remove(tmpPath..".part")
          iup.Flush()
          dlgProgress:destroy()
          return false
           
        end

      end
       

        
    end

  until not c
  dlgProgress:destroy()
  pipe:close()
 return true 
end

popin = {}
popin.ytdl = function(tubeLink, tmpPath)
local flag =  DL_progress(tubeLink, tmpPath)
  if not flag then return false else return true end
end

