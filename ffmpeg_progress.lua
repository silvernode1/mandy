require ('iuplua')





local cancelflag		
local ffmpeg_downloadProgress		

local function downloadStart(YtLink)
  cancelButton = iup.button{
    title = "Cancel",
    action = function()	      
      cancelflag = true         
      end

  }
  
  local ffmpeg_downloadProgress = iup.progressbar{expand="HORIZONTAL", MARQUEE="YES"}	
  local dlgProgress = iup.dialog{				   
  title = "converting",
  dialogframe = "YES", border = "YES",
  iup.vbox {			
    ffmpeg_downloadProgress,
    cancelButton,
    }
  }
  dlgProgress.size = "QUARTERxEIGHT" 		
  dlgProgress.menubox = "NO"		
  dlgProgress.close_cb = cancelButton.action	
  dlgProgress:showxy(iup.CENTER, iup.CENTER)	
  return dlgProgress
end


local function DL_progress(YtLink, tmpPath) 
  downloadStart() 		  
  local pipe = io.popen(YtLink)
  
  
  
  repeat
       c = pipe:read(1) 		
       		     
        iup.LoopStep() 
      --  iup.Message("Terminated","You have failed") 

      if cancelflag then  
       iup.Message("Terminated", "You have canceled the conversion") 
       iup.Flush()
       pipe:close()
       iup.Flush()
       dlgProgress:destroy()
       return false 
      end
    
    until not c
  dlgProgress:destroy()
  pipe:close()
  os.remove(tmpPath)
  return true
end

convert_mp3 = {}
convert_mp3.go = function(tubeLink, tmpPath)
local flag =  DL_progress(tubeLink, tmpPath)
if not flag then return false else return true end
end


