#!/usr/bin/env lua
require("mandy")
dofile("mandy.cfg")
function main()
     local url=global.url()
   if not url then return false end
 --   codec=sel_Codec()
   local saveDir=global.dir()
   if not saveDir then return false end
   
   local tmpPath = global.path(url)
   if not tmpPath then return false end
     
   local convert = global.ffmpeg(tmpPath, saveDir)
end

--initialize function "main"
main()

